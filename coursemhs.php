<?php
session_start();
include 'Koneksi.php';
$user = $_SESSION['user'];
    $sql = "SELECT * FROM mahasiswa WHERE Username='$user'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_array($result)) {
            $ID = $row['ID'];
            $Nama = $row['Nama_Mahasiswa'];
        }
    }else {
        echo "isi SQL kosong";
    }
$user = $_SESSION['user'];
if (!isset($_SESSION['user_is_logged_in']) || $_SESSION['user_is_logged_in'] !== true) {
    header('Location: loginmahasiswa.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Eksternal CSS -->
    <link rel="stylesheet" type="text/css" href="style.css" />
    <!-- Icon Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<nav class="navbar fixed-top navbar-expand-sm bg-success navbar-dark">
  <h4 class="mr-auto" style="color:white">&nbsp Haidar Learn &nbsp</h4>
  <h5 style="color:white"><?php echo $Nama?></h5>
</nav>
<div class="isi">
  <div class="row">
    <div class="col-sm-2 sidebar" style="position:fixed">
      <ul class="nav nav-pills flex-column">
        <li class="nav-item">
          <a class="nav-link" href="homemhs.php">Dashboard</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="profilemhs.php">Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="active" href="#">Course</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="nilaimhs.php">Nilai</a>
        </li>
        <br>
      </ul>
      <a href="logout.php" class="btn btn-outline-success logout">Log Out</a>
    </div>
    <div class="col-sm-8 container">
        <!-- Isi konten -->
        <div class="row course">
        <?php
                $sql = "SELECT * FROM tugasdosen";
                $result = mysqli_query($conn,$sql);
                if(mysqli_num_rows($result) > 0){
                while($data = mysqli_fetch_array($result)){
                    $IDT = $data['ID'];
                    $judul = $data['Nama_Tugas'];
                    $Deskripsi = $data['Deskripsi'];
                    $File = $data['File'];
                    $prog = mysqli_query($conn,"SELECT * FROM uploadjawaban WHERE IDTugas=$IDT");
                    $totalup = mysqli_num_rows($prog);
                    $prog1 = mysqli_query($conn,"SELECT * FROM mahasiswa");
                    $mhs = mysqli_num_rows($prog1);
        ?>
        <div class="col-sm-6" style="margin-top:30px">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title"><?php echo $judul ?></h5>
                <p class="card-text"><?php echo $Deskripsi?></p>
                <a href="download.php?file=<?php echo $File?>" class="btn btn-secondary" role="button">Download Soal</a>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#upload<?php echo $IDT?>">
                  Upload
                </button>
                <!-- Modal -->
                <div class="modal fade" id="upload<?php echo $IDT?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Submit Tugas</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div>
                      <form method="POST" action="upload.php" enctype="multipart/form-data">
                        <input type="hidden" value="<?php echo $data['ID']?>" name="IDT">
                        <input type="hidden" value="<?php echo $ID?>" name="ID"> 
                        <input class="form-control" type="file" name="upload"/>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                          <button type="submit" class="btn btn-success" name="submit">Upload</button>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                </div>

                <?php
                  $prog2 = "SELECT * FROM uploadjawaban WHERE IDTugas=$IDT AND ID_Pengumpul=$ID";
                  $proses = mysqli_query($conn,$prog2);
                  if(@mysqli_num_rows($proses) > 0){
                    $status = "Selesai";
                    ?>
                    <a href="#" class="btn btn-success" role='button'><?php echo $status?></a>
                  <?php
                  }else{
                    $status = "Belum Selesai";
                    ?>
                    <a href="#" class="btn btn-danger" role='button'><?php echo $status?></a>
                  <?php
                  }
                ?>
                
            </div>
            </div>
        </div>
        <?php
                }
                }else{
                    $sql = "ALTER Table  mahasiswa Auto_Increment = 0";
                    $result = mysqli_query($conn,$sql);
                }
        ?>
        </div>
    </div>
  </div>
</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>