<?php
session_start();
include 'koneksi.php';
$user = $_SESSION['user'];
    $sql = "SELECT * FROM dosen WHERE Username='$user'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_array($result)) {
            $ID = $row['ID'];
            $Nama = $row['Nama_Dosen'];
        }
    }else {
        echo "isi SQL kosong";
    }
$user = $_SESSION['user'];
if (!isset($_SESSION['user_is_logged_in']) || $_SESSION['user_is_logged_in'] !== true) {
    header('Location: logindosen.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Eksternal CSS -->
    <link rel="stylesheet" type="text/css" href="style.css" />
    <!-- Icon Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!--Script CSS Table-->
    <link type="text/css" href='https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css' rel='stylesheet'>
    <link type="text/css" href='https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css' rel='stylesheet'>
    <link type="text/css" href='https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css' rel='stylesheet'>
    <title>Document</title>
</head>
<body>
<nav class="navbar fixed-top navbar-expand-sm bg-success navbar-dark">
  <h4 class="mr-auto" style="color:white">&nbsp Haidar Learn &nbsp</h4>
  <h5 style="color:white"><?php echo $Nama?></h5>
</nav>
<div class="isi">
  <div class="row">
    <div class="col-sm-2 sidebar" style="position:fixed">
      <ul class="nav nav-pills flex-column">
        <li class="nav-item">
          <a class="nav-link" href="homedosen.php">Dashboard</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="profiledos.php">Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="active" href="coursedos.php">Course</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="mahasiswados.php">Mahasiswa</a>
        </li>
        <br>
      </ul>
      <a href="logout.php" class="btn btn-outline-success logout">Log Out</a>
    </div>
    <div class="col-sm-8 container" id="dashboard">
        <!--Isi Konten-->
        <br>
        <table id="example" class="display responsive nowrap" style="width:100%">
            <thead>
            <tr align="center">  
                <th>NRP</th>
                <th>NAMA</th>
                <th>File Tugas</th>
                <th>Nilai</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody class="alert-success">
            <?php
                $IDT = $_GET['IDT'];
                $sql = "SELECT * FROM uploadjawaban WHERE IDTugas=$IDT";
                $result = mysqli_query($conn,$sql);
                if(mysqli_num_rows($result) > 0){
                while($data = mysqli_fetch_array($result)){
                    $IDT = $data['IDTugas'];
                    $NamaF = $data['Nama_File'];
                    $IDMHS = $data['ID_Pengumpul'];
                    $Nilai = $data['Nilai'];
                    $file = $data['File'];
                    $sql1 = "SELECT * FROM mahasiswa WHERE ID=$IDMHS";
                    $result1 = mysqli_query($conn,$sql1);
                    if(mysqli_num_rows($result1) > 0){
                        while($program = mysqli_fetch_array($result1)){
                            $IDM = $program['ID'];
                            $NRP = $program['NRP'];
                            $NamaMHS = $program['Nama_Mahasiswa'];
                        }
                    
            ?>
            <tr>
                <td><?php echo $NRP?></td>
                <td><?php echo $NamaMHS?></td>
                <td align="center">
                    <a href="download.php?file=<?php echo $file?>" class="btn btn-primary" role="button">Download Tugas</a>
                </td>
                <td align="center"><?php echo $Nilai?></td>
                <td align="center">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#upload<?php echo $IDMHS?>">
                        Beri Nilai
                    </button>
                </td>
            </tr>
            <!-- Modal -->
            <div class="modal fade" id="upload<?php echo $IDMHS?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Penilaian</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div>
                      <form method="POST" action="inputnilai.php" enctype="multipart/form-data">
                        <input type="hidden" value="<?php echo $IDT?>" name="IDT">
                        <input type="hidden" value="<?php echo $IDMHS?>" name="ID"> 
                        <input class="form-control" style="width:70%; margin-left:15px" type="text" name="input" placeholder="Input Nilai...."/>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                          <button type="submit" class="btn btn-success" name="submit">Input</button>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                </div>
            <?php
                }
            }
            }else{
                $sql = "ALTER Table  mahasiswa Auto_Increment = 0";
                $result = mysqli_query($conn,$sql);
            }
            ?>
            </tbody>
        </table>
        <br>
        <a href="coursedos.php" class="btn btn-secondary">kembali</a> 
    </div>
  </div>
</div>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!--Script Table JS Cuy-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
    <script>
    $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
        'colvis'
        ]
    } );
    } );
    </script>
</body>
</html>