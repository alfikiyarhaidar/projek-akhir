<?php
session_start();
include 'koneksi.php';
$user = $_SESSION['user'];
    $sql = "SELECT * FROM dosen WHERE Username='$user'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_array($result)) {
            $ID = $row['ID'];
            $Nama = $row['Nama_Dosen'];
            $TTL = $row['TTL'];
            $TL = $row['TempatLahir'];
            $Alamat = $row['Alamat'];
            $Email = $row['Email'];
            $Agama = $row['Agama'];
            $Hobi = $row['Hobi'];
            $Pendidikan = $row['Pendidikan'];
            $NRP = $row['NIP'];
            $HP = $row['HP'];
            $gender = $row['Gender'];
            $Foto = $row['Foto'];
            if($Foto==NULL){
              if($gender=="Laki-Laki")
                  $Foto = 'male.PNG';
              else
                  $Foto = 'male.PNG';
            }
        }
    }else {
        echo "isi SQL kosong";
    }
$user = $_SESSION['user'];
if (!isset($_SESSION['user_is_logged_in']) || $_SESSION['user_is_logged_in'] !== true) {
    header('Location: logindosen.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Eksternal CSS -->
    <link rel="stylesheet" type="text/css" href="style.css" />
    <!-- Icon Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<nav class="navbar fixed-top navbar-expand-sm bg-success navbar-dark">
  <h4 class="mr-auto" style="color:white">&nbsp Haidar Learn &nbsp</h4>
  <h5 style="color:white"><?php echo $Nama?></h5>
</nav>
<div class="isi">
  <div class="row">
    <div class="col-sm-2 sidebar" style="position:fixed">
      <ul class="nav nav-pills flex-column">
        <li class="nav-item">
          <a class="nav-link" href="homedosen.php">Dashboard</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="profiledos.php">Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="active" href="coursedos.php">Course</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="mahasiswados.php">Mahasiswa</a>
        </li>
        <br>
      </ul>
      <a href="logout.php" class="btn btn-outline-success logout">Log Out</a>
    </div>
    <div class="col-sm-8 container" style="margin-top:70px">
        <!--Isi Konten-->
        <div id="judul2">
            <h1><b>Tambah Tugas</b></h1>
            <h1><b>Mahasiswa</b></h1>
        </div>
        <br>
        <div class="container" id="course">
            <form action="insertcourse.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="Judul">Judul Tugas</label>
                    <input type="text" name="judul" class="form-control" id="Judul" placeholder="Masukkan Judul Tugas....">
                </div>
                <div class="form-group">
                    <label for="Deskripsi">Deskripsi Tugas</label>
                    <input type="text" name="deskripsi" class="form-control" id="Deskripsi" placeholder="Masukkan Deskripsi Tugas....">
                </div>
                <div class="form-group">
                    <label for="file">Masukkan File Tugas (PDF)</label>
                    <input type="file" name="file" class="form-control" id="file" accept="application/pdf">
                </div>
                <div class="d-flex justify-content-between">
                    <a href="coursedos.php" role="button" class="btn btn-secondary submit">Batal</a>
                    <button type="submit" class="btn btn-primary submit">Add Course</button>
                </div>
            </form>
        </div>
        
    </div>
  </div>
</div>
</body>
</html>