<?php
session_start();
include 'Koneksi.php';
$user = $_SESSION['user'];
    $sql = "SELECT * FROM dosen WHERE Username='$user'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_array($result)) {
            $ID = $row['ID'];
            $Nama = $row['Nama_Dosen'];
        }
    }else {
        echo "isi SQL kosong";
    }
$user = $_SESSION['user'];
if (!isset($_SESSION['user_is_logged_in']) || $_SESSION['user_is_logged_in'] !== true) {
    header('Location: logindosen.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Eksternal CSS -->
    <link rel="stylesheet" type="text/css" href="style.css" />
    <!-- Icon Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<nav class="navbar fixed-top navbar-expand-sm bg-success navbar-dark">
  <h4 class="mr-auto" style="color:white">&nbsp Haidar Learn &nbsp</h4>
  <h5 style="color:white"><?php echo $Nama?></h5>
</nav>

<div class="isi">
  <div class="row">
    <div class="col-sm-2 sidebar" style="position:fixed">
      <ul class="nav nav-pills flex-column">
        <li class="nav-item">
          <a class="nav-link" id="active" href="#">Dashboard</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="profiledos.php">Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="coursedos.php">Course</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="mahasiswados.php">Mahasiswa</a>
        </li>
        <br>
      </ul>
      <a href="logout.php" class="btn btn-outline-success logout">Log Out</a>
    </div>

    <div class="col-sm-8 container" id="dashboard">
      <br>
    <h1 class="section-title" align="center">Informasi Terkini</h1>
    <div id="slide" class="carousel slide" data-ride="carousel">
    <ul class="carousel-indicators">
      <li data-target="#slide" data-slide-to="0" class="active"></li>
      <li data-target="#slide" data-slide-to="1"></li>
      <li data-target="#slide" data-slide-to="2"></li>
    </ul>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="PKM.png" alt="Los Angeles" width="1100" height="500">
        <div class="carousel-caption">
          <h3>Pekan Kreatifitas Mahasiswa</h3>
          <p>Salurkan ide dan inovasi bersama tim tangguhmu, Haidar Learn senantiasa membantu</p>
        </div>   
      </div>
      <div class="carousel-item">
        <img src="beasiswa.png" alt="Chicago" width="1100" height="500">
      </div>
      <div class="carousel-item">
        <img src="loker.png" alt="New York" width="1100" height="500">
        <div class="carousel-caption" style="color:black;text-align:right">
          <h3>Loker Kerja</h3>
          <h4>Menyediakan informasi lowongan kerja sesuai jurusan yang relevan</h4>
        </div>   
      </div>
    </div>
    <a class="carousel-control-prev" href="#slide" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#slide" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
    </div>
    <br>
    <br>

    <div class="row">
      <div class="col-md-7">
        <h2 class="section-title">Ujian Akhir Semester</h2>
          <p style="text-align:justify">Ujian Akhir Semester (UAS) akan dilakasanakn mulai tanggal 28 Juni 2021
            selama seminggu penuh. Diharapkan mahasiswa mempersiapkan kebutuhan yang diperlukan saat UAS agar memperoleh
            hasil yang maksimal. Selain itu, informasi penting yang perlu diperhatikan adalah UAS dilaksanakan secara 
            Online, sehingga kejujuran mahasiswa sangat ditekankan dalam hal ini.</p>
          <a href="#" class="btn btn-outline-success" role="button">Selengkapnya!</a>
      </div>
      <div class="col-md-3">
          <img width="400" height="250" src="UAS.jpg">
      </div>
    </div>
    <br>
    <br>

    <footer class="text-center text-lg-start" id="footer">
    <div class="container text-center text-md-start mt-5">
      <div class="row mt-3">
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
            <h4 class="text-uppercase fw-bold mb-4">
                Haidar Learn
            </h4>
            <p>
                Kami akan selalu menyediakan pelayanan belajar yang santai dengan menerapkan pengoptimalan mahasiswa dalam dunia kerja.
            </p>
        </div>
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
            <h6 class="text-uppercase fw-bold mb-4">
                Kunjungi Kami
            </h6>
        <a
            class="btn btn-primary btn-floating m-1"
            style="background-color: #3b5998;"
            href="#!"
            role="button"
            ><i class="fab fa-facebook-f"></i>
        </a>
        <a
            class="btn btn-primary btn-floating m-1"
            style="background-color: #dd4b39;"
            href="#!"
            role="button"
            ><i class="fab fa-google"></i>
        </a>

        <a
            class="btn btn-primary btn-floating m-1"
            style="background-color: #ac2bac;"
            href="#!"
            role="button"
            ><i class="fab fa-instagram"></i>
        </a>
        </div>
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
          <h6 class="text-uppercase fw-bold mb-4">
            Kontak
          </h6>
          <p><i class="fas fa-home me-3"></i> Jl Kh Hasan Bajuri, Sidoarjo</p>
          <p>
            <i class="fas fa-envelope me-3"></i> haidarlearn@gmail.com
          </p>
          <p><i class="fas fa-phone me-3"></i> 085808322783</p>
        </div>
      </div>
    </div>
    </footer>
    <br>
    <br>
  </div>
</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>