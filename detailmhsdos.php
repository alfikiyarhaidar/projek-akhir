<?php
session_start();
include 'Koneksi.php';
$user = $_SESSION['user'];
$sql = "SELECT * FROM dosen WHERE Username='$user'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_array($result)) {
            $ID = $row['ID'];
            $NamaDosen = $row['Nama_Dosen'];
        }
    }else {
        echo "isi SQL kosong";
    }
if (!isset($_SESSION['user_is_logged_in']) || $_SESSION['user_is_logged_in'] !== true) {
    header('Location: logindosen.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Eksternal CSS -->
    <link rel="stylesheet" type="text/css" href="style.css" />
    <!-- Icon Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
    <!--Script CSS Table-->
    <link type="text/css" href='https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css' rel='stylesheet'>
    <link type="text/css" href='https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css' rel='stylesheet'>
    <link type="text/css" href='https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css' rel='stylesheet'>
</head>
<body>
<nav class="navbar fixed-top navbar-expand-sm bg-success navbar-dark">
  <h4 class="mr-auto" style="color:white">&nbsp Haidar Learn &nbsp</h4>
  <h5 style="color:white"><?php echo $NamaDosen?></h5>
</nav>

<div class="isi">
  <div class="row">
    <div class="col-sm-2 sidebar" style="position:fixed">
      <ul class="nav nav-pills flex-column">
        <li class="nav-item">
          <a class="nav-link" href="homedosen.php">Dashboard</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="profiledos.php">Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Course</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="active" href="#">Mahasiswa</a>
        </li>
        <br>
      </ul>
      <a href="logout.php" class="btn btn-outline-success logout">Log Out</a>
    </div>

    <div class="col-sm-8 container profil">
    <?php 
        $ID = $_GET['id'];
        $sql = "SELECT * FROM mahasiswa WHERE ID=$ID";
        $result = mysqli_query($conn, $sql);
        $row = mysqli_fetch_assoc($result);
            $NRP = $row['NRP'];
            $Nama = $row['Nama_Mahasiswa'];
            $Tempat = $row['TempatLahir'];
            $TGL = $row['TTL'];
            $Alamat = $row['Alamat'];
            $HP = $row['HP'];
            $Gender = $row['Gender'];
            $Email = $row['Email'];
            $Agama = $row['Agama'];
            $Foto = $row['Foto'];
            $Hobi = $row['Hobi'];
            $Pendidikan = $row['Pendidikan'];
    ?>
        <div id="judul2">
            <h1><b>Identitas Pribadi</b></h1>
            <h1><b>Mahasiswa</b></h1>
        </div>
        <div id="form1">
        <table align="center" id="table2">
            <tr>
                <td rowspan="11" width="20" align="center"><img src= "<?php echo $Foto ?>" width="210px" height="280px"></td>
            </tr>
            <tr>
                <td>&nbspNRP</td>
                <td>:&nbsp <?php echo $NRP?></td>
            </tr>
            <tr>
                <td>&nbspNama</td>
                <td>:&nbsp <?php echo $Nama?></td>
            </tr>
            <tr>
                <td>&nbspTTL</td>
                <td>:&nbsp <?php echo $Tempat?>, <?php echo $TGL?></td>
            </tr>
            <tr>
                <td>&nbspAlamat</td>
                <td>:&nbsp <?php echo $Alamat?></td>
            </tr>
            <tr>
                <td>&nbspHP</td>
                <td>:&nbsp <?php echo $HP?></td>
            </tr>
            <tr>
                <td>&nbspJenis Kelamin</td>
                <td>:&nbsp <?php echo $Gender?></td>
            </tr>
            <tr>
                <td>&nbspEmail</td>
                <td>:&nbsp <?php echo $Email?></td>
            </tr>
            <tr>
                <td>&nbspAgama</td>
                <td>:&nbsp <?php echo $Agama?></td>
            </tr>
            <tr>
                <td>&nbspHobi</td>
                <td>:&nbsp <?php echo $Hobi?></td>
            </tr>
            <tr>
                <td>&nbspPendidikan</td>
                <td>:&nbsp <?php echo $Pendidikan?></td>
            </tr>
        </table>
        </div>
        <a href="mahasiswados.php" class="btn btn-primary update">Kembali</a>
    </div>
</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>