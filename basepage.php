<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Eksternal CSS -->
    <link rel="stylesheet" type="text/css" href="style.css" />
    <!-- Icon Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Base Page</title>
</head>
<body>
<!--Navbar Cuy-->
<header class="header">
<nav class="navbar navbar-expand-lg navbar-dark bg-success">
    <h4 style="color:white">Haidar Learn &nbsp</h4>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
 
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#"> Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Program Belajar</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Riwayat</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#bottom">Hubungi Kami</a>
        </li>
      </ul>
      <div class="dropdown" style="padding-right:60px">
        <a class="btn btn-outline-light mr-2 dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Login
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="loginmahasiswa.php">Mahasiswa</a>
            <a class="dropdown-item" href="logindosen.php">Dosen</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Tamu</a>
        </div>
      </div>
   </div>
</nav>
</header>
<main class="container main">
  <br>
<div id="demo" class="carousel slide" data-ride="carousel">
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="E-learning.jpg" alt="Los Angeles" width="1100" height="500">
      <div class="carousel-caption">
        <h3>Model Belajar</h3>
        <p>Kami menerapkan model pembelajaran jarak jauh (PJJ) dengan adanya E-Learning</p>
      </div>   
    </div>
    <div class="carousel-item">
      <img src="meeting.jpg" alt="Chicago" width="1100" height="500">
      <div class="carousel-caption">
        <h3>Meet Conference</h3>
        <p>Fasilitas konferensi online yang lengkap untuk mendukung PJJ</p>
      </div>   
    </div>
    <div class="carousel-item">
      <img src="pengajar.jpg" alt="New York" width="1100" height="500">
      <div class="carousel-caption">
        <h3>Dosen Berkualitas</h3>
        <p>Dengan adanya dosen yang berkualitas tinggi berbanding lurus dengan kualitas mahasiswa</p>
      </div>   
    </div>
  </div>
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
<br><br>
<div class="row">
  <div class="col-md-7">
    <h2 class="section-title">Telah berdiri 50 tahun</h2>
      <p style="text-align:justify">Haidar Learn telah hadir selama 50 tahun lebih dalam dunia keprofesian dan pendidikan swasta di Indonesia. 
      Tentunya dengan kualitas, pelayanan, dan Biaya yang sesuai kebutuhan calon mahasiswa. Dengan pengalaman yang cukup lama menemani Indonesia,
      Haidar Learn tentu memiliki kapabilitas yang mumpuni dalam mencetak mahasiswa Indonesia yang berkualitas dan beretika dalam menghadapi masa mendatang.</p>
      <a href="#" class="btn btn-outline-success" role="button">Selengkapnya!</a>

  </div>
  <div class="col-md-3">
    <img width="300" height="300" src="50 tahun.jpg">
  </div>
</div>
</main>
<br><br><br>

<!--Footer Cuy-->
<section id="bottom">
<footer class="text-center text-lg-start" id="footer">
    <div class="container text-center text-md-start mt-5">
      <div class="row mt-3">
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
            <h3 class="text-uppercase fw-bold mb-4">
                Haidar Learn
            </h3>
            <p>
                Kami akan selalu menyediakan pelayanan belajar yang santai dengan menerapkan pengoptimalan mahasiswa dalam dunia kerja.
            </p>
        </div>
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
            <h5 class="text-uppercase fw-bold mb-4">
                Kunjungi Kami
            </h5>
        <a
            class="btn btn-primary btn-floating m-1"
            style="background-color: #3b5998;"
            href="#!"
            role="button"
            ><i class="fab fa-facebook-f"></i>
        </a>
        <a
            class="btn btn-primary btn-floating m-1"
            style="background-color: #dd4b39;"
            href="#!"
            role="button"
            ><i class="fab fa-google"></i>
        </a>

        <a
            class="btn btn-primary btn-floating m-1"
            style="background-color: #ac2bac;"
            href="#!"
            role="button"
            ><i class="fab fa-instagram"></i>
        </a>
        <a
            class="btn btn-primary btn-floating m-1"
            style="background-color: #0082ca;"
            href="#!"
            role="button"
            ><i class="fab fa-linkedin-in"></i>
        </a>
        <a
            class="btn btn-primary btn-floating m-1"
            style="background-color: #333333;"
            href="#!"
            role="button"
            ><i class="fab fa-github"></i>
        </a>
        <a
            class="btn btn-primary btn-floating m-1"
            style="background-color: #55acee;"
            href="#!"
            role="button"
            ><i class="fab fa-twitter"></i>
        </a>
        </div>
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
          <h5 class="text-uppercase fw-bold mb-4">
            Kontak
          </h5>
          <p><i class="fas fa-home me-3"></i> Jl Kh Hasan Bajuri, Sidoarjo</p>
          <p>
            <i class="fas fa-envelope me-3"></i> haidarlearn@gmail.com
          </p>
          <p><i class="fas fa-phone me-3"></i> 085808322783</p>
        </div>
      </div>
    </div>
</footer>
</section>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>