<?php
session_start();
$errorMessage = '';
if (isset($_POST['username']) && isset($_POST['sandi'])){
    include 'koneksi.php';
    $user = $_POST['username'];
    $password = $_POST['sandi'];
    $_SESSION['user'] = $user;
    $sql = "SELECT Username FROM dosen
            WHERE Username = '$user' AND `Password` ='$password'";
    $result = mysqli_query($conn,$sql);

    if (mysqli_num_rows($result) == 1) {
        $_SESSION['user_is_logged_in'] = true;
        header('Location: homedosen.php?user=$user');
        exit;
    }else {
        $errorMessage = 'Maaf, username atau password tidak sesuai!!!';
    }
    mysqli_close($conn);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Eksternal CSS -->
    <link rel="stylesheet" type="text/css" href="style.css" />
    <!-- Icon Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Page</title>
</head>
<body>
<div class="container" id="form">
    <div class="logo-mycustome">
        <em class="fas fa-user"></em>
    </div>
    <form action="" method="POST">
        <h1 align="center">Login</h1>
        <h2 align="center">Dosen</h2>
        <div class="form-group">
            <label for="user">Username</label>
            <input type="text" class="form-control" name="username" placeholder="Enter Username">
            <small class="form-text text-muted">*Masukkan username dengan valid</small>
        </div>
        <div class="form-group">
            <label for="sandi">Password</label>
            <input type="password" class="form-control" name="sandi" placeholder="*************">
            <small class="form-text text-muted">*Password anda tidak akan ditampilkan</small>
        </div>
        <?php
            if($errorMessage!=''){
                echo "<p class='form-text text-muted' align='center'>" .$errorMessage. "</p>";
            }
        ?>
        <br>
        <button type='submit' class='btn btn-block btn-mycustome'>Login</button>
    </form>
</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>