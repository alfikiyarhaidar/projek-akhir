<?php
session_start();
include 'Koneksi.php';
$user = $_SESSION['user'];
    $sql = "SELECT * FROM dosen WHERE Username='$user'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_array($result)) {
            $ID = $row['ID'];
            $Nama = $row['Nama_Dosen'];
        }
    }else {
        echo "isi SQL kosong";
    }
$user = $_SESSION['user'];
if (!isset($_SESSION['user_is_logged_in']) || $_SESSION['user_is_logged_in'] !== true) {
    header('Location: logindosen.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Form Pendaftaran</title>
</head>
<body id="BG">
    <div class="container" id="judul">
			<h1><b>Registrasi</b></h1>
			<h3><b>Tambah Mahasiswa Baru</b></h3>
	</div>
    <br>
    <div class="container" id="form4">
        <form action="insertmhs.php" method="post" enctype="multipart/form-data">
            <div class="col-md-6" style="border-right: 2px solid">
                <div class="form-group">
                    <label for="" class="form-label">NRP</label>
                    <input type="number" class="form-control" name="NRP" placeholder="Masukkan NRP Anda">
                </div>
                <div class="form-group">
                    <label for="name" class="form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama" placeholder=" Masukkan Nama Anda....">
                </div>
                <div class="form-group">
                    <label for="TTL" class="control-label col-xs-2" style="padding-left:0;">TTL</label>
                    <div class="col-xs-5">
                        <input type="text" class="form-control" name="tempat" placeholder="Tempat">
                    </div>
                    <div class="col-xs-5">
                        <input type="date" class="form-control" name="TGL">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alamat" class="form-label">Alamat</label>
                    <input type="text" class="form-control" name="alamat" placeholder="Masukkan alamat anda....">
                </div>
                <div class="form-group">
                    <label for="HP" class="form-label">No. Handphone</label>
                    <input type="number" class="form-control" name="HP" placeholder=" Masukkan No. Hp Anda....">
                </div>
                <div class="form-group">
                    <label for="mail" class="form-label">Email</label>
                    <input type="email" class="form-control" name="mail" placeholder=" Masukkan email Anda....">
                </div>
                <div class="form-group">
                    <label for="JK" class="control-label col-xs-4" style="padding-left:0;">Jenis Kelamin</label>
                    <label class="radio-inline col-xs-3"><input type="radio" name="gender" value="Laki-Laki">Laki-laki</label>
                    <label class="radio-inline col-xs-3"><input type="radio" name="gender" value="Perempuan">Perempuan</label>
                </div>
                <br>
            </div>
        
            <div class="col-md-6">
                <div class="form-group">
                        <label class="control-label col-xs-3" style="padding:0">Agama</label>
                        <div class="col-xs-4">
                            <select class="form-control" name="agama">
                                <option>Agama</option>
                                <?php
                                    $rel=array('Islam','Protestan','Katolik','Hindu',"Budha");
                                    foreach($rel as $religi){
                                        echo "<option value='{$religi}'>{$religi}</option>";
                                    }
                                ?>
                            </select>
                        </div>
                </div>
                <br>
                <div class="form-group">
                    <label for="Hobby" class="form-label">Foto Pribadi</label>
                    <input type="file" class="form-control" name="gambar" accept="image/*" placeholder="JPG/PNG">
                </div>
                <div class="form-group">
                    <label for="Hobby" class="form-label">Hobi</label>
                    <input type="text" class="form-control" name="Hobi" placeholder=" Masukkan hobi Anda....">
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3" style="padding:0">Studi</label>
                    <div class="col-xs-4">
                        <select class="form-control" name="Semester">
                            <option>Program</option>
                            <option value="D3">D3</option>
                            <option value="D4">D4</option>
                            <option value="S1">S1</option>
                        </select>
                    </div>
                    <div class="col-xs-5">
                        <select name="jurusan" class="form-control">
                            <option>Jurusan</option>
                            <?php
                                $jur = array('T. Informatika','T. Elektronika','T. Elektronika Industri','T. Mekatronika','Multimedia Broadcasting','T. Telekomunikasi','Teknik Game','T. Komputer','S. Pembangkit Energi');
                                foreach($jur as $jurusan){
                                    echo"<option value='{$jurusan}'>{$jurusan}</option>";
                                }
                            ?> 
                        </select>
                    </div>
                </div>
                <br><br>
                <div class="form-group">
                    <label for="mail" class="form-label">Username</label>
                    <input type="text" class="form-control" name="username" placeholder=" Masukkan username Anda....">
                </div>
                <div class="form-group">
                    <label for="mail" class="form-label">Password</label>
                    <input type="password" class="form-control" name="password">
                </div>
                <button type="submit" class="btn btn-primary btn-lg submit">Tambahkan</button>
            </div>
        </form>
    </div>
    <a href="mahasiswados.php" class="btn btn-info" role="button" style="margin-left:270px">< Kembali</a>
</body>
</html>