<?php
session_start();
include 'Koneksi.php';
$user = $_SESSION['user'];
    $sql = "SELECT * FROM dosen WHERE Username='$user'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_array($result)) {
            $ID = $row['ID'];
            $Nama = $row['Nama_Dosen'];
        }
    }else {
        echo "isi SQL kosong";
    }
$user = $_SESSION['user'];
if (!isset($_SESSION['user_is_logged_in']) || $_SESSION['user_is_logged_in'] !== true) {
    header('Location: logindosen.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Eksternal CSS -->
    <link rel="stylesheet" type="text/css" href="style.css" />
    <!-- Icon Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<nav class="navbar fixed-top navbar-expand-sm bg-success navbar-dark">
  <h4 class="mr-auto" style="color:white">&nbsp Haidar Learn &nbsp</h4>
  <h5 style="color:white"><?php echo $Nama?></h5>
</nav>
<div class="isi">
  <div class="row">
    <div class="col-sm-2 sidebar" style="position:fixed">
      <ul class="nav nav-pills flex-column">
        <li class="nav-item">
          <a class="nav-link" href="homedosen.php">Dashboard</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="profiledos.php">Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="active" href="#">Course</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="mahasiswados.php">Mahasiswa</a>
        </li>
        <br>
      </ul>
      <a href="logout.php" class="btn btn-outline-success logout">Log Out</a>
    </div>
    <div class="col-sm-8 container">
        <!-- Isi konten -->
        <div class="d-flex justify-content-between course">
            <a href="addcourse.php" class="btn btn-info" role="button">Add Course</a>
        </div>

        <div class="row course1">
        <?php
                $sql = "SELECT * FROM tugasdosen";
                $result = mysqli_query($conn,$sql);
                if(mysqli_num_rows($result) > 0){
                while($data = mysqli_fetch_array($result)){
                    $IDT = $data['ID'];
                    $judul = $data['Nama_Tugas'];
                    $Deskripsi = $data['Deskripsi'];
                    $prog = mysqli_query($conn,"SELECT * FROM uploadjawaban WHERE IDTugas=$IDT");
                    $totalup = mysqli_num_rows($prog);
                    $prog1 = mysqli_query($conn,"SELECT * FROM mahasiswa");
                    $mhs = mysqli_num_rows($prog1);
        ?>
        <div class="col-sm-6" style="margin-top:30px">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title"><?php echo $judul ?></h5>
                <p class="card-text"><?php echo $Deskripsi?></p>
                <p>Terkumpul : <?php echo $totalup."/".$mhs?></p>
                <a href="detailcoursedos.php?IDT=<?php echo $IDT?>" class="btn btn-secondary" role="button">Detail Terkumpul</a>
                <a href="courseupdate.php?IDT=<?php echo $IDT?>" class="btn btn-primary" role="button">Update</a>
            </div>
            </div>
        </div>
        <?php
                }
                }else{
                    $sql = "ALTER Table  mahasiswa Auto_Increment = 0";
                    $result = mysqli_query($conn,$sql);
                }
        ?>
        </div>
    </div>
  </div>
</div>
</body>
</html>
