<?php
session_start();
include 'Koneksi.php';
$user = $_SESSION['user'];
    $sql = "SELECT * FROM mahasiswa WHERE Username='$user'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_array($result)){
            $ID = $row['ID'];
            $Nama = $row['Nama_Mahasiswa'];
            $TTL = $row['TTL'];
            $TL = $row['TempatLahir'];
            $Alamat = $row['Alamat'];
            $Email = $row['Email'];
            $Agama = $row['Agama'];
            $Hobi = $row['Hobi'];
            $Pendidikan = $row['Pendidikan'];
            $NRP = $row['NRP'];
            $HP = $row['HP'];
            $gender = $row['Gender'];
            $Foto = $row['Foto'];
            if($Foto==NULL){
              if($gender=="Laki-Laki")
                  $Foto = 'male.PNG';
              else
                  $Foto = 'male.PNG';
            }
            $pass = $row['Password'];
        }
    }else {
        echo "isi SQL kosong";
    }
$user = $_SESSION['user'];
if (!isset($_SESSION['user_is_logged_in']) || $_SESSION['user_is_logged_in'] !== true) {
    header('Location: loginmahasiswa.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Update Data</title>
</head>
<body id="BG">
    <div class="container" id="judul">
		<h1><b>UPDATE DATA</b></h1>
        <h2><b><?php echo $Nama ?></b></h2>
		<h3><b>NRP : <?php echo $NRP?></b></h3>
	</div>
    <br>
    <div class="container" id="form3">
        <form action="Update.php" method="post" enctype="multipart/form-data">
        <input type="hidden" value="<?php echo $ID?>" name="ID">
            <div class="col-md-6" style="border-right: 2px solid">
                <div class="form-group">
                    <label for="" class="form-label">NRP</label>
                    <input type="number" class="form-control" name="NRP" value="<?php echo $NRP?>" placeholder="Masukkan NRP Anda">
                </div>
                <div class="form-group">
                    <label for="name" class="form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama" value="<?php echo $Nama?>" placeholder=" Masukkan Nama Anda....">
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3" style="padding:0">Agama</label>
                    <div class="col-xs-4">
                        <select class="form-control" name="agama">
                            <option>Agama</option>
                            <?php
                                $rel=array('Islam','Protestan','Katolik','Hindu',"Budha");
                                    foreach($rel as $religi){
                                        if($religi==$Agama){
                                            echo "<option value='{$religi}' selected>{$religi}</option>";
                                        }else{
                                            echo "<option value='{$religi}'>{$religi}</option>";
                                        }

                                    }
                            ?>
                        </select>
                    </div>
                </div>
                <br><br><br>
                <div class="form-group">
                    <label for="TTL" class="control-label col-xs-2" style="padding-left:0;">TTL</label>
                    <div class="col-xs-5">
                        <input type="text" class="form-control" name="tempat" value="<?php echo $TL?>" placeholder="Tempat">
                    </div>
                    <div class="col-xs-5">
                        <input type="date" class="form-control" name="TGL" value="<?php echo $TTL?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="mail" class="form-label">Email</label>
                    <input type="text" class="form-control" name="mail" value="<?php echo $Email?>" placeholder="Masukkan Email anda....">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="alamat" class="form-label">Alamat</label>
                    <input type="text" class="form-control" name="alamat" value="<?php echo $Alamat?>" placeholder="Masukkan alamat anda....">
                </div>
                <div class="form-group">
                    <label for="JK" class="control-label col-xs-3" style="padding-left:0;">Jenis Kelamin</label>
                    <?php
                        if($gender== "Laki-Laki"){
                            echo "<label class='radio-inline col-xs-2'><input type='radio' name='gender' value='Laki-Laki' checked>Laki-laki</label>";
                            echo "<label class='radio-inline col-xs-3'><input type='radio' name='gender' value='Perempuan'>Perempuan</label>";
                        }
                        else{
                            echo "<label class='radio-inline col-xs-2'><input type='radio' name='gender' value='Laki-Laki'>Laki-laki</label>";
                            echo "<label class='radio-inline col-xs-3'><input type='radio' name='gender' value='Perempuan' checked>Perempuan</label>";
                        }
                            
                    ?>
                </div>
                <br><br>
                <div class="form-group">
                    <label for="Hobby" class="form-label">Foto Pribadi</label>
                    <input type="file" class="form-control" name="gambar" accept="image/*" placeholder="JPG/PNG">
                </div>
                <div class="form-group">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" name="username" value="<?php echo $user?>" placeholder="Masukkan user anda....">
                </div>
                <div class="form-group">
                    <label for="passwrod" class="form-label">Password</label>
                    <input type="password" class="form-control" name="password" value="<?php echo $pass?>" placeholder="Masukkan password anda....">
                </div>
                <button type="submit" class="btn btn-primary btn-lg submit">Update</button>
            </div>
        </form>
    </div>
    <a href="profilemhs.php" class="btn btn-info" role="button" style="margin-left:270px">< Kembali</a>
</body>
</html>